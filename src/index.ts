import express from "express";
import companyRoutes from "../src/company/companyRoutes";
import fileUpload from "express-fileupload";
import userRoutes from "../src/user/userRoutes";
import reportRoutes from "../src/Report/reportRoutes";
import { DB } from "./database/Connection";


DB.init();

const app = express();
const PORT = 3000;

//set json parser urlencoded
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(fileUpload());

//database connection

//routers
app.use("/user", userRoutes);
app.use("/company", companyRoutes);
app.use("/report", reportRoutes);

app.listen(PORT, () => {
    console.log("The application is listening on port 3000!");
});

