import { CompanyController } from "./companyController";

import express from "express";
const router = express.Router();
const companyController = new CompanyController(); 

router.post("/add-company", companyController.insertCompany);
router.get("/get-company/details", companyController.getCompanyDetails);
router.get("/get-company-detail/:companyId", companyController.getDetailsWithId);
router.post("/update-company-details/:Id/:companyId", companyController.updateCompanyDetails);

export = router;