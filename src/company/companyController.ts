import { Request, Response } from "express";
import { CompanyUtils } from "./companyUtils";
const My = require("jm-ez-mysql");

export class CompanyController {
    private companyUtils: CompanyUtils = new CompanyUtils();

    //add company
    public insertCompany = async (req: Request, res: Response) => {
        const data = await this.companyUtils.insertCompany(req.body);
        res.send(data);
    }

    //get all company details
    public getCompanyDetails = async (req: Request, res: Response) => {
        const data = await this.companyUtils.getCompanyDetails();
        res.send(data);
    }
    
    //update company with user id 
    public updateCompanyDetails = async (req: Request, res: Response) => {
        const data = await this.companyUtils.updateCompanyDetails(req.params.uId, req.params.companyId, req.body);
        res.send(data);
    }

    //get one specific company deatils 
    public getDetailsWithId = async (req: Request, res: Response) => {
        const data = await this.companyUtils.getDetialsWithId(req.params.companyId);
        res.send(data);
    }
}

