const My = require("jm-ez-mysql");

export class CompanyUtils {

    public async insertCompany (reqData: any) {        
        const {uId ,companyName, companyEmail, companyAddress, companyId} = reqData;

        const result = await My.insert("company", reqData);
        return result;
    }

    public async getCompanyDetails () {
        const selectParam = ["c.uId, companyName, companyEmail, companyAddress, userName, userEmail"];
        const joinQuery = `company as c left join user as u on c.uId = u.uId`;

        const result = await My.findAll(`${joinQuery}`, selectParam);
        return result;
    }

    public async updateCompanyDetails (uId: any, companyId: any, reqData: any) {
        const where = `uId = ${uId} and companyId = ${companyId}`;
        const {companyName, companyEmail, companyAddress} = reqData;

        const result = await My.update("company", reqData, `${where}`);
        return result;
    }

    public async getDetialsWithId (companyId: any) {
        const selectParam = ["c.uId, companyName, companyEmail, companyAddress, userName, userEmail"];
        const joinQuery = `company as c left join user as u on c.uId = u.uId`;
        const where = `companyId = ${companyId}`;

        const result = await My.findAll(`${joinQuery}`, selectParam, `${where}`);
        return result;
    }
}