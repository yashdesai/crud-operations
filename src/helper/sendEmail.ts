import * as nodemailer from "nodemailer";
import * as dotenv from "dotenv";
dotenv.config()

export class SendEmails {

    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.AUTHUSER,
            pass: process.env.AUTHPASS
        }
    });
     
}