import mysql from "mysql";
import dotenv from "dotenv";
const jmEzMySql  = require("jm-ez-mysql");
dotenv.config();

export class DB {
  public static init() {
    jmEzMySql.init({
      host: process.env.DBHOST,
      user: process.env.DBUSER,
      password: process.env.DBPASSWORD,
      database: process.env.DATABASE,
    });
  }
}
