import { Request, Response } from "express";
import { ReportUtils } from "./reportUtils";
import { DownloadReports } from "./downloadReports";

export class ReportController {
    private reportUtils: ReportUtils = new ReportUtils();
    private downloadReport: DownloadReports = new DownloadReports();

    public genReport = async (req: Request, res: Response) => {

        const {origin, mimeType, generateReport} = req.body;
        const filter = {origin, mimeType, generateReport};

        const result = await this.reportUtils.report(filter);
        if(generateReport === true || generateReport === "true"){
            const data = result;
            if(mimeType === "xlsx"){
                const workbook = this.downloadReport.genExcelReport(data, origin);
                res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                await workbook.xlsx.write(res);
            } else {
                res.send("please select MimeType");
            }
        } else {
            res.json(result);
        }
    }
}