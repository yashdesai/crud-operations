import { ReportController } from "./reportController";
import express from "express";

const router = express.Router();
const reportController = new ReportController();

router.post("/get-user/report", reportController.genReport);

export = router;