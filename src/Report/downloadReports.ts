import * as excel from "exceljs";
import * as _ from "lodash";

export class DownloadReports {

    public genExcelReport (reportResponse: any, origin: any) {
        const responseD: any = [];

        if(reportResponse && reportResponse.length > 0) {
            const result: any = [];
            const emptyResult: any = [];
            reportResponse.forEach((ele: any) => {
                if(ele.tasks && ele.length > 0) {
                    result.push(ele);
                } else {
                    emptyResult.push(ele);
                }
            });
            reportResponse = _.concat(result, emptyResult);
        }
        responseD.push(reportResponse);
        const cols: any = [];
        origin.forEach((ori: any) => {
            if(ori.isSet) {
                cols.push({header: ori.name, key: ori.field});
            }
        });
        return this.exportAsExcelFile(responseD, cols);
    }


    public exportAsExcelFile (json: any[], origin:  any) {
        const workbook = new excel.Workbook();
        if(json.length > 0) {
            json.forEach((element: any) => {
                const worksheet = workbook.addWorksheet(element.name);

                worksheet.columns = origin;
                worksheet.addRows(element);
            });
        } else {
            const worksheet = workbook.addWorksheet("No Data");

            worksheet.columns = [{header: "No-Data", key: "No-Data"}];
            worksheet.addRows([{"No-Data" : "No-Data"}]);
        }
        return workbook;
    }
}