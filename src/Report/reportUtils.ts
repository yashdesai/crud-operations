const My = require("jm-ez-mysql");

export class ReportUtils {

    public async report (object: any) {
        const joinQuery = `user left join company on user.uId = company.uId`
        const selectParams =["user.uId, userName, userEmail, companyName, companyEmail, companyId"];

        const result = await My.findAll(`${joinQuery}`, selectParams);
        return result;
    }
}