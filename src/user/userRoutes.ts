import { UserController } from "./userController";

import express from "express";
const router = express.Router();
const userController = new UserController();


router.post("/add-user/insert", userController.insertUser);
router.post("/update-user/:uId", userController.updateUser);
router.delete("/delete-user/:uId", userController.deleteUser);
router.get("/get-user/details", userController.getDetails);
router.get("/get-user-detail/:uId", userController.getUserDetailsWithId);
router.delete("/delete-company/:uId/:companyId", userController.deleteCompany);
router.get("/get-details/inner-join", userController.innerJoinQuery);
router.get("/get-details/right-join", userController.rightJoinQuery);
router.get("/get-details/cross-join", userController.crossJoinQuery);
router.post("/bulk-upload/insert", userController.bulkUpload);

export = router;