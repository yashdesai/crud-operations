import { Request, Response } from "express";
import { UserUtils } from "./userUtils";
import { UploadedFile } from "express-fileupload";
import * as dotenv from "dotenv";
import render from "xlsx";
import path from "path";
import { SendEmails } from "../helper/sendEmail";
dotenv.config();

// Insert data 
export class UserController {
    private userUtils: UserUtils = new UserUtils();
    private sendEmails: SendEmails = new SendEmails();
    
    public insertUser = async (req: Request, res: Response) => {
        if(!req.files){
            res.send("file not uploaded");
        }
        const file = req.files?.profile_img as UploadedFile;
        const fileName = file.name.split(`.`);
        const fName = `profile_img` + Math.floor(Math.random()*101) + `.` + fileName[1];
    
        const checkEmail = await this.userUtils.checkEmail(req.body.userEmail);
        if(req.body.userEmail === checkEmail.userEmail){
            res.send("Email already exist.")
        } else {

            const data = await this.userUtils.addUser({
                uId: req.body.uId,
                userName: req.body.userName,
                userEmail: req.body.userEmail,
                profile_img: fName
            });
            const mailOptions = {
                from: process.env.AUTHUSER,
                to: req.body.userEmail,
                subject: 'Welcome',
                text: 'User created successfully'
            };
           
            this.sendEmails.transporter.sendMail(mailOptions, (err, info) => {
                if(err) {
                    res.send(err)
                } else {
                    res.send('Email sent: ' + info.response);
                }
            })
        }
    }

    //update user
    public updateUser = async(req: Request, res: Response) => {
        const data = await this.userUtils.updateUser(req.body, req.params.uId);
        res.send(data);
    }

    //delete user
    public deleteUser = async (req:Request, res: Response) => {
        const data = await this.userUtils.deleteUser(req.params.uId);
        res.send(data);
    }

    //get all the details of user with company table
    public getDetails = async (req: Request, res: Response) => {
        const data = await this.userUtils.getDetails();
        res.send(data);
    }

    //get all details of one specific user
    public getUserDetailsWithId = async (req: Request, res: Response) => {
        const data = await this.userUtils.getUserDetailsWithId(req.params.uId);
        res.send(data);
    }

    //delete company
    public deleteCompany = async (req: Request, res: Response) => {
        const data = await this.userUtils.deleteCompany(req.params.uId, req.params.companyId);
        res.send(data);
    }
    
    //inner join 
    public innerJoinQuery = async (req: Request, res: Response) => {
        const data = await this.userUtils.innerJoin();
        res.send(data);
    }

    //right join
    public rightJoinQuery = async (req: Request, res: Response) => {
        const data = await this.userUtils.rightJoin();
        res.send(data);
    }

    //cross join
    public crossJoinQuery = async (req: Request, res: Response) => {
        const data = await this.userUtils.crossJoin();
        res.send(data);
    }

    public bulkUpload = async (req: Request, res: Response) => {
        const dirPath = path.join(__dirname, "../");
        const uploadFile = req.files?.uploadFile as UploadedFile;
        const extension = path.extname(uploadFile.name);
        
        if(extension !== ".xls"){
            res.send("Select valid file extension ");
        } else {    
            const URL: any = path.join(dirPath + "/uploads/" + uploadFile.name);
            uploadFile.mv(URL,async (err) => {
                if(err) {
                    return res.send(err);
                } else{
                    const file = render.readFile(URL)
                    const sheets = file.SheetNames;
                    const data: any = [];
                    try{
                        for (let i = 0; i < sheets.length; i++) {
                            const sheetsName = sheets[i];
                            const sheetData = render.utils.sheet_to_json(file.Sheets[sheetsName]);
                            sheetData.forEach((ele) => {
                                data.push(ele);
                            })
                        }
                        const result = await this.userUtils.bulkUpload(data);
                        res.send(result);
                    } catch(err) {
                        res.send("Failed to upload")
                    }
                }
            });
        }
       
    }
}
