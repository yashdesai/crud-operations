const My = require("jm-ez-mysql");
export class UserUtils {

    public async addUser (reqData: any) {
        const {uId, userName, userEmail, profile_img} = reqData;
        const result = await My.insert("user", reqData);
        return result
    }

    public async checkEmail (userEmail: any) {
        const where = `userEmail = ?`;
            const selectParam = ["userEmail"]

            const email = My.first("user", selectParam, `${where}`, [userEmail]);
            return email;
    }

    public async updateUser (reqData: any, uId: any) {
        const {userName, userEmail} = reqData;
        const where = `uId = ${uId}`;

        const result = await My.update("user", reqData, `${where}`);
        return result;
    }

    public async deleteUser (uId: any) {
        const where = `company.uId = ${uId}`;
        const whereTwo = `uId = ${uId}`
        const selectParam = ["company.uId, company.companyName, company.companyEmail, company.companyId"];
        // console.log(where);
        
        const result = await My.findAll("company", selectParam, `${where}`) 
        console.log(result);
          
        if(uId == result[0].uId){
            console.log("User can't be deleted.User have to first remove it's Meta data.");
        } else {
            const result = await My.delete("user", `${whereTwo}`);
            console.log("deleted");
        }
    } 

    public async getDetails () {
        const selectParam = ["u.uId , userName, userEmail, companyName, companyEmail, companyId"];
        const joinQuery = `user as u left join company as c on u.uId = c.uId`;

        const result = await My.findAll(`${joinQuery}`, selectParam);
        return result;
    }

    public async getUserDetailsWithId (uId: any) {
        const selectParam = ["u.uId , userName, userEmail, companyName, companyEmail, companyId"];
        const joinQuery = `user as u left join company as c on u.uId = c.uId`;
        const where = `u.uId = ${uId}`;

        const result = await My.findAll(`${joinQuery}`, selectParam, `${where}`);
        return result;
    }

    public async deleteCompany (uId: any, companyId: any) {
        const where = `companyId = ${companyId} and uId = ${uId}`;

        const result = await My.delete("company", `${where}`);
        return result;
    }

    public async innerJoin () {
        const selectParams = ["userName, companyName, companyEmail"];
        const joinQuery = `user as u inner join company as c on u.uId = c.uId`;

        const result = await My.findAll(`${joinQuery}`, selectParams);
        return result;
    }

    public async rightJoin () {
        const selectParam = ["userName, userEmail, companyName"];
        const joinQuery =  `user as u right join company as c on u.uId = c.uId`;

        const result = await My.findAll(`${joinQuery}`, selectParam);
        return result;
    }

    public async crossJoin () {
        const selectParam = ["userName, userEmail, companyName, companyEmail"];
        const joinQuery = `user cross join company`;

        const result = await My.findAll(`${joinQuery}`, selectParam);
        return result;
    }

    public async bulkUpload (reqData: any[]) {
        const columnData = reqData[0];
        const inputColumnLength = Object.keys(columnData).length;
        const data = await My.findAll("user",["*"]);
        const columnDataFromTable = data[0];
        const tableColumnLength = Object.keys(columnDataFromTable).length;
        try{
            if(inputColumnLength > tableColumnLength) {
                return "Error"
            } else {
                const result = await My.insertMany("user", reqData);
                return result;
            }
        } catch(err) {
            return "Invalid data.";
        }
    }
}